<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <label for="account">ログインID</label> <input name="account" id="account" /><br />
                <label for="password1">パスワード（登録)</label> <input name="password1" type="password" id="password1" /><br /> 
                 <label for="password2">パスワード（確認)</label> <input name="password2" type="password" id="password2" /><br /> 
                <label for="name">氏名</label> <input name="name" id="name" /> <br />
                
                <label for="branch">支店</label>
                <select name="branch" id="branch">
                	<c:forEach items="${branch}" var="br">
                		<option value="${br.id}">${br.name}</option>
                	</c:forEach>
                </select><br />
                
                <label for="position">部署・役職</label>
              <select name="position" id="position">
                	<c:forEach items="${position}" var="po">
                		<option value="${po.id}">${po.name}</option>
                	</c:forEach>
                </select><br />
               
                <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
        </div>
    </body>
</html>