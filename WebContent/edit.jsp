<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html >
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>編集</title>
    </head>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            
            <form action="	edit" method="post">
          		<input type="hidden" value="${edit.id}" name="id" />
                <label for="account">ログインID</label> <br />
                <input name="account" id="account" value="${edit.account}"/><br />
                <label for="password1">パスワード（変更）</label><br />
                 <input name="password1" type="password" id="password1" /><br /> 
                 <label for="password2">パスワード（確認)</label> <br />
                 <input name="password2"type="password" id="password2" /><br /> 
                <label for="name">氏名</label><br />
                 <input name="name" id="name"value="${edit.name}" /> <br />
                
                <label for="branch">支店</label><br />
                <select name="branch" id="branch" >
                	<c:forEach items="${branch}" var="br">
                		<c:if test="${br.id == edit.branch}">
                			<option value="${br.id}"selected>${br.name}</option>
                		</c:if>
                		<c:if test="${br.id != edit.branch}">
                			<option value="${br.id}">${br.name}</option>
                		</c:if>
                	</c:forEach>
                </select><br />
                
                <label for="position">部署・役職</label><br />
               <select name="position" id="position" >
                	<c:forEach items="${position}" var="po">
                		<c:if test="${po.id == edit.position}">
                			<option value="${po.id}" selected>${po.name}</option>
                		</c:if>
                		<c:if test="${po.id != edit.position}">
                			<option value="${po.id}">${po.name}</option>
                		</c:if>
                	</c:forEach>
                </select><br />
  
                <input type="submit" value="編集" /> <br /> <a href="./">戻る</a>
            </form>
        </div>
</html>