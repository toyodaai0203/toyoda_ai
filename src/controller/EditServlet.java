package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Branch;
import beans.Info;
import beans.Position;
import beans.User;
import service.BranchService;
import service.InfoService;
import service.PositionService;
import service.UserService;


@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		int value =Integer.parseInt(request.getParameter("Id"));
		Info edit = new InfoService().getEdit(value);
		request.setAttribute("edit", edit);
		
	    List<Branch> branch = new BranchService().getBranch();
		request.setAttribute("branch", branch);
		  
		List<Position> position = new PositionService().getPosition();
		request.setAttribute("position", position);
		  
		
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}


	@Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
    	

        List<String> messages = new ArrayList<String>();
        List<Branch> branch = new BranchService().getBranch();
        List<Position> position = new PositionService().getPosition();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);        
       
                
        if (isValid(request, messages) == true) {
            new UserService().update(editUser);
            response.sendRedirect("./");
        } else {
            request.setAttribute("branch",branch);
            request.setAttribute("position", position);
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("/edit.jsp").forward(request, response);
        }
    }
		
            private User getEditUser(HttpServletRequest request) 
            	throws IOException, ServletException {
	
			User editUser = new User();
            editUser.setId(Integer.parseInt(request.getParameter("id")));
            editUser.setName(request.getParameter("name"));
            editUser.setAccount(request.getParameter("account"));
            editUser.setPassword1(request.getParameter("password1"));
            editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
            editUser.setPosition(Integer.parseInt(request.getParameter("position")));
            return editUser;
        }

        
    

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("account");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");
        
        Info edit = new InfoService().getAccount(account);

        if (!account.matches("^[a-zA-Z0-9]{6,20}$")) {
            messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください。");
        }
        if (edit != null ) {
        messages.add("既に存在するアカウントです。");
        }
        if (password1.isEmpty() == true) {
        } else if (!password1.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
            messages.add("パスワードは半角文字(記号含む)6文字以上20文字以下で入力してください");
        } else if (!password2.equals(password1)) {
        	messages.add("パスワードが一致しません");
    	}
    	if (name.length() > 10) {
            messages.add("氏名を10文字以下で入力してください。");
        }
    	
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }

    }
}
