package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import beans.Info;
import exception.SQLRuntimeException;

public class InfoDao extends HttpServlet {

    public List<Info> select(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("branches.name as branch_name,");
            sql.append("positions.name as position_name,");
            sql.append("users.created_date as created_date ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position = positions.id ");
            

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Info> ret = toInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
 
	}

	private List<Info> toInfoList(ResultSet rs)
			throws SQLException {

        List<Info> ret = new ArrayList<Info>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                int id = rs.getInt("id");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Info info = new Info();
                info.setAccount(account);
                info.setName(name);
                info.setPassword(password);
                info.setId(id);
                info.setBranch_name(branchName);
                info.setPosition_name(positionName);
                info.setCreated_date(createdDate);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

	public Info getEdit(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.branch as branch, ");
	            sql.append("users.position as position, ");
	            sql.append("users.account as account, ");
	            sql.append("users.password as password, ");
	            sql.append("users.name as name, ");
	            sql.append("branches.name as branch_name, ");
	            sql.append("positions.name as position_name, ");
	            sql.append("users.created_date as created_date ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branches ");
	            sql.append("ON users.branch = branches.id ");
	            sql.append("INNER JOIN positions ");
	            sql.append("ON users.position = positions.id ");
	            sql.append("WHERE users.id = ?");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<Info> userList = toUserEditList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Info> toUserEditList(ResultSet rs)
            throws SQLException {

        List<Info> ret = new ArrayList<Info>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int branch = rs.getInt("branch");
                int position = rs.getInt("position");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                Timestamp createdDate = rs.getTimestamp("created_date");                
                Info info = new Info();
                info.setId(id);
                info.setBranch(branch);
                info.setPosition(position);
                info.setAccount(account);
                info.setPassword(password);
                info.setName(name);
                info.setBranch_name(branchName);
                info.setPosition_name(positionName);
                info.setCreated_date(createdDate);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

	public Info getAcount(Connection connection, String account) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.account as account ");
	            sql.append("FROM users ");
	            sql.append("WHERE users.account = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setString(1, account);

	        ResultSet rs = ps.executeQuery();
	        List<Info> userList = toUserAccountList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<Info> toUserAccountList(ResultSet rs)
            throws SQLException {

        List<Info> ret = new ArrayList<Info>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");

                Info info = new Info();
                info.setAccount(account);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

}