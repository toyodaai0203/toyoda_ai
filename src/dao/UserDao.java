package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
	
	public class UserDao{

    	 public void insert(Connection connection, User user) {

    	        PreparedStatement ps = null;
    	        try {
    	            StringBuilder sql = new StringBuilder();
    	            sql.append("INSERT INTO users ( ");
    	            sql.append("account");
    	            sql.append(", name");
    	            sql.append(", branch");
    	            sql.append(", password");
    	            sql.append(", position");
    	            sql.append(", created_date");
    	            sql.append(", updated_date");
    	            sql.append(") VALUES (");
    	            sql.append("?"); // account
    	            sql.append(", ?"); // name
    	            sql.append(", ?"); // branch
    	            sql.append(", ?"); // password
    	            sql.append(", ?"); // position
    	            sql.append(", CURRENT_TIMESTAMP"); // created_date
    	            sql.append(", CURRENT_TIMESTAMP"); // updated_date
    	            sql.append(")");

    	            ps = connection.prepareStatement(sql.toString());

    	            ps.setString(1, user.getAccount());
    	            ps.setString(2, user.getPassword1());
    	            ps.setString(3, user.getName());
    	            ps.setInt(4, user.getBranch());
    	            ps.setInt(5, user.getPosition());
    	            ps.executeUpdate();
    	        } catch (SQLException e) {
    	            throw new SQLRuntimeException(e);
    	        } finally {
    	            close(ps);
    	        }
    	
    	 }
    	 
    	
    	 
    	 public void update(Connection connection, User user) {
    		 PreparedStatement ps = null;
    	        try {
    		StringBuilder sql = new StringBuilder();
  	 			sql.append("UPDATE users SET");
  	 			sql.append(" account = ?");
	            sql.append(", name = ?");
	            sql.append(", branch = ?");
	            sql.append(", position = ?");
	            sql.append(", created_date = CURRENT_TIMESTAMP");
	            sql.append(", updated_date = CURRENT_TIMESTAMP");
	            if(StringUtils.isEmpty(user.getPassword1()) == false){
	            	sql.append(", password = ?");
	            }
	            sql.append(" WHERE");
	            sql.append(" id = ?"); 
	                    
  	 		
     	 		ps = connection.prepareStatement(sql.toString());
     	 		
     	 		ps.setString(1, user.getAccount());
     	 		ps.setString(2, user.getName());
     	 		ps.setInt(3, user.getBranch());
     	 		ps.setInt(4, user.getPosition());
     	 		if(StringUtils.isEmpty(user.getPassword1()) == false){
                	ps.setString(5, user.getPassword1());
                	ps.setInt(6, user.getId());
                } else {
                	ps.setInt(5, user.getId());
                }

     	 		
     	 		int count = ps.executeUpdate();
     	 		if (count == 0) {
     	 			throw new NoRowsUpdatedRuntimeException();
     	 		}
     	 	} catch (SQLException e) {
     	 		throw new SQLRuntimeException(e);
     	 	} finally {
     	 		close(ps);
     	 	}
     	 }
	}
    	
 
	


