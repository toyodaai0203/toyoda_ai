package dao;
import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Position;
import exception.SQLRuntimeException;
	
	public class PositionDao{


		public List<Position>select(Connection connection) {

    	        PreparedStatement ps = null;
    	        try {
    	            StringBuilder sql = new StringBuilder();
    	            sql.append("SELECT ");
    	            sql.append("positions.id as id,");
    	            sql.append("positions.name as name ");
    	            sql.append("FROM positions ");

    	           
    	            ps = connection.prepareStatement(sql.toString());

    	   
    	            ResultSet rs = ps.executeQuery();
    	            List<Position>ret = toPositionList(rs);
    	            return ret;
    	            
    	        } catch (SQLException e) {
    	            throw new SQLRuntimeException(e);
    	        } finally {
    	            close(ps);
    	        }
    	
    	 }
		private List<Position> toPositionList(ResultSet rs)
	            throws SQLException {

	        List<Position> ret = new ArrayList<Position>();
	        try {
	            while (rs.next()) {
	                String name = rs.getString("name");
	                int id = rs.getInt("id");

	                Position position = new Position();
	                position.setName(name);
	                position.setId(id);

	                ret.add(position);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	    }

	}