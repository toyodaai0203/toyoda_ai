package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import beans.Info;
import dao.InfoDao;

public class InfoService extends HttpServlet {
		public List<Info> getInfo() {


        Connection connection = null;
        try {
            connection = getConnection();

            InfoDao infoDao = new InfoDao();
            List<Info> ret = infoDao.select(connection);

            commit(connection);
            
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		
    }

		public Info getEdit(int value) {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            InfoDao InfoDao = new InfoDao();
	            Info user = InfoDao.getEdit(connection, value);

	            commit(connection);

	            return user;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }

		public Info getAccount(String value) {
			 Connection connection = null;
		        try {
		            connection = getConnection();

		            InfoDao InfoDao = new InfoDao();
		            Info user = InfoDao.getAcount(connection, value);

		            commit(connection);

		            return user;
		        } catch (RuntimeException e) {
		            rollback(connection);
		            throw e;
		        } catch (Error e) {
		            rollback(connection);
		            throw e;
		        } finally {
		            close(connection);
		        }
		    }


		}
