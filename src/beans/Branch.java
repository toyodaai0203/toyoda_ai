package beans;

import java.util.Date;



public class Branch  {

private int id;
private Date updatedDate;
private Date createdDate;
private String name;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public Date getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(Date updatedDate) {
	this.updatedDate = updatedDate;
}
public Date getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
       

	


}