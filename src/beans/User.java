package beans;

import java.util.Date;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;



@WebServlet("/User")
public class User extends HttpServlet {
	private static final long serialVersionUID = 1L;
private String account;
private int id;
private String password1;
private String password2;
private String name;
private int branch;
private int position;
private Date updatedDate;
private Date createdDate;
public String getAccount() {
	return account;
}
public void setAccount(String account) {
	this.account = account;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getPassword1() {
	return password1;
}
public void setPassword1(String password1) {
	this.password1 = password1;
}
public String getPassword2() {
	return password2;
}
public void setPassword2(String password2) {
	this.password2 = password2;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getBranch() {
	return branch;
}
public void setBranch(int branch) {
	this.branch = branch;
}
public int getPosition() {
	return position;
}
public void setPosition(int position) {
	this.position = position;
}
public Date getUpdatedDate() {
	return updatedDate;
}
public void setUpdatedDate(Date updatedDate) {
	this.updatedDate = updatedDate;
}
public Date getCreatedDate() {
	return createdDate;
}
public void setCreatedDate(Date createdDate) {
	this.createdDate = createdDate;
}
       


}